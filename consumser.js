var querystring = require('querystring');
var http = require('http');
var sleep = require('sleep');
var host = process.env.omer_host || "127.0.0.1";
var port = process.env.omer_port || 8081;
var MAX_JOBS=process.env.MAX_JOBS || 10;
var SUCCESS_RATE = process.env.SUCCESS_RATE || 50;
var request = require('sync-request');
var queue = "all";
function getTask() {

    var responseString = '';
    var responseObject = null;
  
    if(process.argv.length==3)
	queue = process.argv[2];

    var res = request('GET', "http://" + host + ":" + port + "/" + "Tasks?queue=" + queue, { });

    var res_object = JSON.parse(res.getBody('utf8'));
    //    console.log("WORKING ON " , res_object);
    return res_object;
}

function updateFailedTask(obj) {

    var responseString = '';
    var body = JSON.stringify(obj);

    var res = request('PUT', "http://" + host + ":" + port + "/" + "Tasks/" + obj._id + "/Failed" , {
            json: JSON.parse(body)
        });
}


function updateCompletedTask(obj) {

    var responseString = '';
    var body = JSON.stringify(obj);

    var res = request('PUT', "http://" + host + ":" + port + "/" + "Tasks/" + obj._id + "/Completed" , {
            json: JSON.parse(body)
        });


}
var counter=0;
var process_job = function(){
    
	var response_object = getTask();
	
	if(response_object != null)
	    performTask(response_object);
	else
	    setTimeout(process_job,3000);
    //    success = 1;
 
}
    var performTask = function (response_object){
	console.log("performing id=%s, sleeping for %s try = %s " , response_object._id,response_object.duration, (response_object.attempts+1));

    setTimeout( function(){
	    
	    success = Math.random() < (SUCCESS_RATE/100) ? 1 : 0 ;
	   
	    if(success == 1)
		{
		    updateCompletedTask(response_object);
		    console.log("SUCCESSFULLY COMPLETED %s", response_object._id);
		}
	    
	    else
		{
		    console.log("FAILED %s", response_object._id);
		    updateFailedTask(response_object);
		}

	    process_job();

	},response_object.duration*1000);

}


var i = 0;

while(i++<MAX_JOBS)
    {
	process_job();
    }
