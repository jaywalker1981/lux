var mongodb = require('mongodb');
var util = require('util');
var async = require('async-waterfall');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var host = process.env.omer_mongo_host || "ds015335.mlab.com";
var port = process.env.omer_mongo_port || 15335;
var user = process.env.omer_mongo_login || "luxe";
var pw = process.env.omer_mongo_pw || "luxe";

//macros = require 'macros';
var find_in_queue =  function(db,str_collection, callback){

    var collection = db.collection(str_collection);    	   
    var seconds = Math.round(new Date() / 1000);
    console.log(str_collection);
    collection.findAndModify({$or:[{picked:false},{"SLA_limit":{$lt:seconds}}]},
{},
{ $set:{picked:true}},
{},
			     function(err, result){
				 
				 if(err)
				     {
					 console.log(err);
					 callback(null,true,{});
				     }
				 else
				     {
					 
					 if(result.value == null){
					     callback(null, true,{});
					 }
					 else{
					     
					     var seconds = Math.round(new Date() / 1000);
					     seconds = seconds + result.value.SLA;
					     collection.findAndModify(
								      {_id : result.value._id},
								      {},
								      { $set:{SLA_limit:seconds }},
								      {},function(err,result){});

					     callback(null,false,result.value);
					     
					 }
				     }
			     });
}
    var retrieve = function(res,queue){
	
	
	var result = {};
	
	async([
	       function(callback){
		   if(queue === '1' || queue ==='all')
		       find_in_queue(db,"first_attempt",callback);
		   else
		       callback(null,true,result);
	       },
	       function(cont,result,callback){
		   
		   if(cont==true)
		       {
			   if(queue === '2' || queue ==='all')
			       find_in_queue(db,"second_attempt",callback);
			   else
			       callback(null,cont,result);
		       }
		   else
			       callback(null,cont,result);
	       },
	       function(cont,result,callback){
		   
		   if(cont==true)
		       {
			   if(queue === '3' || queue ==='all')
			       find_in_queue(db,"third_attempt",callback);
			   else
			       callback(null,cont,result);
		       }
		   else
		       callback(null,cont,result);

	       }], function(err,cont,result){
		  
		  
		  if(cont === true){
		      result = null;
		      //	  res.end();
		  }
		  
		  //		      else
		  res.end( JSON.stringify(result));		      
		  // db.close();
		  
	      });
		    
    }
	
 
 
	
	var updateFailedTasks = function(id, duration, attempts,SLA){
	    updateCompletedTasks(id, attempts - 1);
	    
			
			var str_attempt;
			if(attempts===0)
			    str_attempt = "first_attempt";
			if(attempts===1)
		str_attempt = "second_attempt";
			else if(attempts===2)
			    str_attempt = "third_attempt";
			else if(attempts===3)
			    str_attempt = "failed";

			var collection = db.collection(str_attempt);
			
			
			var job = {"_id": new mongodb.ObjectID(id), "duration":duration,"attempts" : attempts , "picked" : false , "SLA":SLA};
			// Insert some users
			collection.insert([job], function x(err, result) {
				if (err) {
			console.log(err);
				} else {
				    console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
				}
				//Close connection
				//    db.close();
			    });
			
	}

	   
	    var db;
var server;
	    var setupDB = function(){
		var MongoClient = mongodb.MongoClient;
		
		var url = "mongodb://" + user + ":" + pw + "@" + host + ":" + port + "/luxe";
		console.log(url);
		MongoClient.connect(url, function (err, temp_db) {
			if (err) {
			    console.log('Unable to connect to the mongoDB server. Error:', err);
			} else {
			    db = temp_db;
			    
			    var server = app.listen(8081, function () {
				    
				    var host = server.address().address
				    var port = server.address().port
				    
				    console.log("servicelistening at http://%s:%s", host, port)
				    
				});
			    
			}
		    });
	    }
		    
	var updateCompletedTasks = function(id, attempts)
	{
	    
	    
	    var str_attempt;
	    if(attempts==0)
		str_attempt = "first_attempt";
	    if(attempts==1)
		str_attempt = "second_attempt";
	    else if(attempts==2)
		str_attempt = "third_attempt";
	    
	    var collection = db.collection(str_attempt);
	    
	    var job = {"_id": new mongodb.ObjectID(id)};
	    // Insert some users
	    collection.deleteMany(job , function (err, result) {
		    if (err) {
			console.log(err);
		    } else {
			
		    }
		    //Close connection
		    //		    db.close();
		});
	}
		    
		    var newTask = function(duration,attempt, SLA){
			    var str_attempt;
			    if(attempt===0)
				str_attempt = "first_attempt";
			    if(attempt===1)
				str_attempt = "second_attempt";
			    else if(attempt===2)
				str_attempt = "third_attempt";
			    
			    var collection = db.collection(str_attempt);
			    
			    //Create some users
			    
			    var job = {"duration":duration , "picked" : false , "attempts" : 0 , "SLA" : SLA};
			    // Insert some users
			    collection.insert([job], function x(err, result) {
				    if (err) {
					console.log(err);
				    } else {
					console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
				    }
		    //Close connection
				    //				    db.close();
				});
		    }


		
app.use(bodyParser.json());

app.get('/Tasks', function (req, res) {
	console.log(req.query.queue);
	retrieve(res,req.query.queue);
    });

app.put('/Tasks/:_id/Completed', function (req, res) {
	
	var req_object = JSON.parse(JSON.stringify(req.body));
	updateCompletedTasks(req.params._id, req_object.attempts);
	res.end();
    });

app.put('/Tasks/:_id/Failed', function (req, res) {
	
	var req_object = JSON.parse(JSON.stringify(req.body));
	    updateFailedTasks(req.params._id, req_object.duration, req_object.attempts + 1, req_object.SLA);
	    res.end();
    });

app.post('/Tasks', function (req, res) {
	var req_object = JSON.parse(JSON.stringify(req.body));
	   res.end();
	   newTask(req_object.duration,0,req_object.SLA);
       });


		    setupDB();
